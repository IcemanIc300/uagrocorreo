<!DOCTYPE html>
<html>
<head>
    <title>Pagina Principal</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../assets/css/SideBar.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/Header.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/Container.css">
    <link rel="stylesheet" href="../assets/css/reset.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
    <link rel="stylesheet" href="../assets/css/main.css">
</head>
<body>

<div id="header">
    <div>
        <h1>
            Escribir un Nuevo Correo
        </h1>
    </div>
</div>

<div id="sidebar">
    <div class="widget">
        <div class="title">
            <h3>
                Navegación
            </h3>
            <ul>
                <li>
                    <a href="Principal.php">Menu Principal</a>
                </li>
            </ul>
        </div>
        <br><br><br>
        <div class="login__top">
            <img src="../assets/img/escribir.jpg" height="200" width="200"/>
            <h2 class="login__title">Usuario:<span> Uagro</span></h2>
        </div>
    </div>
</div>

<div id="container">
    <div>
        <div class="container">
            <div class="form__top">
                <h2><span>Redactar Correo</span></h2>
            </div>
            <form class="form__reg" action="../Controlador/EnviarMsg.php" method="post">
                <input id="Asunto" name="Asunto" class="input" type="text" placeholder="Escriba el Asunto" required autofocus>
                <input id="Destinatario" name="Destinatario" class="input" type="email" placeholder="Correo del Destinatario" required>
                <textarea id="Mensaje" name="Mensaje" cols="20" rows="10" placeholder="Escriba el Mensaje"></textarea>
                <?php
                session_start();
                if(isset($_SESSION['Id'])) {
                    echo "<input type='hidden' name='Id' id='Id' value='$_SESSION[Id]'>";
                }else{
                    header('Location: ../index.html');
                }

                ?>
                <div class="btn__form">
                    <input class="btn__submit" type="submit" value="ENVIAR">
                    <input class="btn__reset" type="reset" value="LIMPIAR">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="footer">
    <h4>Jos&eacute; Mar&iacute;a Vences Carbajal</h4>
</div>

</body>
</html>