<!DOCTYPE html>
<html>
    <head>
        <title>Pagina Principal</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../assets/css/SideBar.css">
        <link rel="stylesheet" type="text/css" href="../assets/css/Header.css">
        <link rel="stylesheet" type="text/css" href="../assets/css/Container.css">
        <link rel="stylesheet" href="../assets/css/reset.css">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
        <link rel="stylesheet" href="../assets/css/main.css">
    </head>
    <body>
        <div id="header">
            <div>
                <h1>
                    Servicio de Correo Electronico Uagro
                </h1>
            </div>
        </div>

        <div id="sidebar">
            <div class="widget">
                <div class="title">
                    <h3>
                        Nuevo
                    </h3>
                    <ul>
                        <li>
                            <a href="Nuevo.php">Nuevo</a>
                        </li>
                    </ul>
                </div>
                <div class="title">
                    <h3>
                        Recibidos
                    </h3>
                    <ul>
                        <li>
                            <a href="Recibidos.php">Recibidos</a>
                        </li>
                    </ul>
                </div>
                <div class="title">
                    <h3>
                        Enviados
                    </h3>
                    <ul>
                        <li>
                            <a href="Enviados.php">Enviados</a>
                        </li>
                    </ul>
                </div>
                <div class="title">
                    <h3>
                        Cerrar Session
                    </h3>
                    <ul>
                        <li>
                            <a href="../Controlador/CerrarSession.php">Cerrar</a>
                        </li>
                    </ul>
                </div>
                <br><br><br>
                <div class="login__top">
                    <img src="../assets/img/user.png" height="200" width="200"/>
                    <h2 class="login__title">Usuario:<span> Uagro</span></h2>
                </div>
            </div>
        </div>

        <div id="container">
            <div>
                <div class="container">
                    <div class="form__top">
                        <h2><span>Información General</span></h2>
                    </div>
                    <form class="form__reg" action="">
                        <?php
                        session_start();
                        if(isset($_SESSION['Id'])) {
                            $mysqli = mysqli_connect("127.0.0.1", "correo_db", "correo_db", "correo_db",3306)
                            or die("Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error);

                            $sSQL="SELECT Nombre, ApellidoPaterno, ApellidoMaterno, Correo, Telefono, Usuario FROM tbl_informacion_usuario
inner JOIN tbl_usuario on tbl_informacion_usuario.IdUsuario = tbl_usuario.IdUsuario
WHERE tbl_informacion_usuario.IdUsuario = $_SESSION[Id]";
                            $resultado = $mysqli->query($sSQL) or trigger_error($mysqli->error);
                            while ($fila = $resultado->fetch_row()) {
                                echo "<input type='hidden' name='Id' id='Id' value='$_SESSION[Id]'>";
                                echo "<input id='Nombre' name='Nombre' value=$fila[0] class='input' type='text' placeholder='&#128100;  Nombre' required autofocus>";
                                echo "<input id='ApellidoPaterno' name='ApellidoPaterno' value=$fila[1] class='input' type='text' placeholder='&#128100;  Apellido Paterno' required autofocus>";
                                echo "<input id='ApellidoMaterno' name='ApellidoMaterno' value=$fila[2] class='input' type='text' placeholder='&#128100;  Apellido Materno' required autofocus>";
                                echo "<input id='Correo' name='Correo' class='input' value=$fila[3] type='email' placeholder='&#9993;  Email' required>";
                                echo "<input id='Telefono' name='Telefono' class='input' type='text' value=$fila[4] placeholder='&#128222;  Telefono' required>";
                                echo "<input id='Usuario' name='Usuario' class='input' type='text' value=$fila[5] placeholder='&#8962;  Usuario' required>";
                            }
                        }else{
                            header('Location: ../index.html');
                        }

                        ?>
                       </form>
                </div>
            </div>
        </div>

        <div id="footer">
            <h4>Jos&eacute; Mar&iacute;a Vences Carbajal</h4>
        </div>

    </body>
</html>