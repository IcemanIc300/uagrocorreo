<!DOCTYPE html>
<html>
<head>
    <title>Pagina Principal</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../assets/css/SideBar.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/Header.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/Container.css">
    <link rel="stylesheet" href="../assets/css/reset.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
    <link rel="stylesheet" href="../assets/css/main.css">
</head>
<body>

<div id="header">
    <div>
        <h1>
            Correos Recibidos
        </h1>
    </div>
</div>

<div id="sidebar">
    <div class="widget">
        <div class="title">
            <h3>
                Navegación
            </h3>
            <ul>
                <li>
                    <a href="Principal.php">Menu Principal</a>
                </li>
            </ul>
        </div>
        <br><br><br>
        <div class="login__top">
            <img src="../assets/img/recibidos.png" height="200" width="200"/>
            <h2 class="login__title">Usuario:<span> Uagro</span></h2>
        </div>
    </div>
</div>

<div id="container">
    <div>
        <div class="container">
            <table>
                <tr>
                    <th>Asunto</th>
                    <th>Mensaje</th>
                    <th>Remitente</th>
                    <th>Fecha</th>
                </tr>
                <?php
                session_start();
                if(isset($_SESSION['Id'])) {
                echo "<input type='hidden' name='Id' id='Id' value='$_SESSION[Id]'>";
                $mysqli = mysqli_connect("127.0.0.1", "correo_db", "correo_db", "correo_db",3306)
                or die("Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error);

                $sSQL="SELECT Asunto, Mensaje, Usuario, Fecha FROM tbl_buzon 
                INNER  JOIN tbl_detalle_buzon ON tbl_buzon.IdBuzon=tbl_detalle_buzon.IdBuzon
                INNER JOIN tbl_usuario ON tbl_detalle_buzon.IdUsuarioDestinatario=tbl_usuario.IdUsuario
                WHERE IdUsuarioDestinatario = $_SESSION[Id]";
                $resultado = $mysqli->query($sSQL) or trigger_error($mysqli->error);

                while ($fila = $resultado->fetch_row()) {
                    echo "<tr> <td>$fila[0]</td> <td>$fila[1]</td> <td>$fila[2]</td> <td>$fila[3]</td> ";
                    echo "</tr>";
                }
                }else{
                    header('Location: ../index.html');
                }

                ?>

            </table>
        </div>
    </div>
</div>

<div id="footer">
    <h4>Jos&eacute; Mar&iacute;a Vences Carbajal</h4>
</div>

</body>
</html>